import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        System.out.println(toPigLatinWord("Halla"));
        System.out.println(toPigLatinWord("be"));
        System.out.println(toPigLatinSentence("Hello my name is Sander!"));
    }

    public static String toPigLatinWord(String inputString)
    {
        String vowels = "AaEeIiOoUuYy";
        StringBuilder stringBuilder = new StringBuilder();

        ArrayList<Character> arrayList = new ArrayList<>();

        for (char character : inputString.toCharArray())
        {
            if (vowels.indexOf(character) != -1)
            {
                if (arrayList.isEmpty())
                {
                    //System.out.println(inputString + "yay");
                    return inputString + "yay";
                }

                for (Character ch : arrayList)
                {
                    stringBuilder.append(ch);
                }
                //System.out.println(inputString + stringBuilder.toString() + "ay");
                return inputString + stringBuilder.toString() + "ay";
            }
            else
            {
                arrayList.add(character);
                inputString = inputString.replaceFirst(Character.toString(character), "");
            }
        }
        //System.out.println(inputString + stringBuilder.toString() + "ay");
        return inputString + stringBuilder.toString() + "ay";
    }

    public static String toPigLatinSentence(String sentence)
    {
        String correctSentence = sentence.replaceAll("[^a-zA-Z0-9]"," ");

        String[] words = correctSentence.split(" ");
        ArrayList<String> newWords = new ArrayList<>();
        for (String word : words)
        {
            newWords.add(toPigLatinWord(word));
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (String word : newWords)
        {
            stringBuilder.append(word);
            stringBuilder.append(" ");
        }
        if (sentence.contains("!"))
        {
            return (stringBuilder.toString().trim() + "!");
        }
        else if (sentence.contains("."))
        {
            return (stringBuilder.toString().trim() + ".");
        }
        else if (sentence.contains("?"))
        {
            return (stringBuilder.toString().trim() + "?");
        }
        return stringBuilder.toString();
    }
}

